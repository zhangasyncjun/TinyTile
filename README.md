# TinyTile
Library for implementing and integrating tile services(TMS WMTS XYZ) in .NET application.

# How to use
## MBTiles

```csharp
var source = new MbTile(path);
var layer = new Layer("points_mbtile_3857",source,gridSet:GridSet.Create(SRS.Epsg3857,schema:YAxisSchema.Xyz));
var tile = layer.GetTile(x, y, z);

var file = File(tile,TileMimes.Pbf);
Response.Headers.Add("Content-Encoding","gzip");
return file;
```
## PostGIS
Suppose Linq2db is the ORM.
```csharp
[Table("points_of_interest", Schema = "public")]
public class Poi
{
    [Column("id"), PrimaryKey, NotNull]
    public int Id { get; set; }
    [Column("name")]
    public string Name { get; set; }
    [Column("kind")]
    public string Kind { get; set; }
    [Column("geom")]
    public Point Geometry { get; set; }
}
```
```csharp
//inject linq2db attributes for discovering table fields
var pgSource = SqlSource.FromTable<TableAttribute,ColumnAttribute,Poi>(connstring,SRS.Epsg4326,t => t.Name,c => c.Name,new string[]{"0,4,0.2","4,8,0.4","8,16,0.5","16,21,1.0"});//at zoom [0,4),only show 20% of data,[4,8),only show 40% of data,etc.

//create layer with tile grid 4326,256*256px,90.7142 dpi,Google XYZ schema.
var layerPg4326 = new Layer("points_pg_4326",pgSource,gridSet:GridSet.Create(SRS.Epsg3857,schema:YAxisSchema.Xyz));

//create layer with tile grid 3857,256*256px,90.7142 dpi,Google XYZ schema.
var layerPg4490 = new Layer("points_pg_4490",pgSource,gridSet:GridSet.Create(SRS.Epsg4490,YAxisSchema.Xyz));

var tile4326 = layerPg4326.GetTile(x, y, z);
var tile4490 = layerPg4490.GetTile(x, y, z);

var file4326 = File(tile4326,TileMimes.Mvt);
var file3857 = File(tile3857,TileMimes.Mvt);

return file4326;
```
## Demo
```bash
cd DemoApp
dotnet run
```
Visit mbtile xyz service,http://localhost:8088/mbtiles@3857@xyz.html
![avatar](./mbtiles3857.png)
Visit PostGIS xyz service, http://localhost:8088/postgis@4490@xyz.html
![avatar](./postgis.png)
# TO DO
* Support other vector format tile,eg geojson,topojson...
* Support various mbtiles(1.3 and epsg3857 only now)
* Support render vector data source to raster tile(png,jpeg...) with styles(mapbox style,sld..)

* Support WMTS
* Support TMS
* Support raster data source
* Support various vector data source,eg PostGIS,Shapefile,GeoJson,Tile files,etc
