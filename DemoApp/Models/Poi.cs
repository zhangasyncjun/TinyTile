using LinqToDB.Mapping;
using NTSG = NetTopologySuite.Geometries.Geometry;

namespace DemoApp.Models
{
    [Table("points_of_interest", Schema = "public")]
    public class Poi
    {
        [Column("id"), PrimaryKey, NotNull]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("kind")]
        public string Kind { get; set; }
        [Column("geom")]
        public NetTopologySuite.Geometries.Point Geometry { get; set; }
    }
}