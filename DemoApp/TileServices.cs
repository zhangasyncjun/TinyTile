using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DemoApp.Models;
using LinqToDB.Mapping;
using TinyTile.Layers;
using TinyTile.Sources;
using TinyTile.Utils;

namespace DemoApp
{
    public class TileServices
    {
        public Dictionary<string,ILayerable> Layers { get; set; } = new Dictionary<string, ILayerable>();
        public TileServices(string connstring)
        {
            var pgSource = SqlSource.FromTable<TableAttribute,ColumnAttribute,Poi>(connstring,SRS.Epsg4326,t => t.Name,c => c.Name,new string[]{"0,4,0.2","4,8,0.4","8,16,0.5","16,21,1.0"});

            var layerPg4326 = new Layer("points_pg_4326",pgSource,gridSet:GridSet.Create(SRS.Epsg3857,schema:YAxisSchema.Xyz));
            Layers.Add(layerPg4326.Name,layerPg4326);

            var layerPg4490 = new Layer("points_pg_4490",pgSource,gridSet:GridSet.Create(SRS.Epsg4490,YAxisSchema.Xyz,minZoom:0,maxZoom:21));
            Layers.Add(layerPg4490.Name,layerPg4490);

            var path = Path.Combine(AssemblyPath, "TestDatas", "points.mbtiles");
            var source = new MbTile(path);
            var layer2 = new Layer("points_mbtile_3857",source,gridSet:GridSet.Create(SRS.Epsg3857,schema:YAxisSchema.Xyz));
            
            Layers.Add(layer2.Name,layer2);
        }
        private static string AssemblyPath
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().Location;
                var dir = Path.GetDirectoryName(codeBase);

                return dir;
            }
        }
    }
}
