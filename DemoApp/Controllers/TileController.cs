using Microsoft.AspNetCore.Mvc;
using TinyTile.Utils;

namespace DemoApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TileController:ControllerBase
    {
        public TileController(TileServices tileServices)
        {
            TileServices = tileServices;
        }

        public TileServices TileServices { get; set; }

        [HttpGet("{layername}/{x}/{y}/{z}.mvt")]
        public IActionResult GetTile(string layerName,int x,int y,int z)
        {
            var layer = TileServices.Layers[layerName];
            var tile = layer.GetTile(x, y, z);
            if(tile == null){
                return NotFound();
            }
            var file = File(tile,TileMimes.Mvt);
            if(layer.Format == TileFormat.PBF)
            {
                Response.Headers.Add("Content-Encoding","gzip");    
            }
            
            return file;
        }
    }
}
