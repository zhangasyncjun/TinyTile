using NUnit.Framework;
using TinyTile.Sources;
using TinyTile.Utils;
using LinqToDB.Mapping;
namespace TInyTile.Tests
{
    [TestFixture]
    public class PostGISTests
    {
        private string connString = "Server=127.0.0.1;Port=5432;Database=postgistest;User Id=reader;Password=reader;CommandTimeout=30;Timeout=15;";
        [Test]
        public void TestSqlSource()
        {
            var sqlSource = new SqlSource(connString,new System.Func<int, int, int, GridSet, string>(SqlFunc),SRS.Epsg4326);
            var tile = sqlSource.GetTile(0,0,0,GridSet.Create(SRS.Epsg3857));
            Assert.IsTrue(tile.Length > 0);
        }

        [Test]
        public void TestSqlSourceFromTable1()
        {
            //todo more tests!
            var t = typeof(TableAttribute);
            var source = SqlSource.FromTable<TableAttribute,ColumnAttribute,Poi>(connString,SRS.Epsg4326,t => t.Name, c => c.Name);
            var tile = source.GetTile(1,0,0,GridSet.Create(SRS.Epsg4326));
            Assert.IsTrue(tile.Length > 0);
        }
        [Test]
        public void TestSqlSourceFromTable2()
        {
            var source = SqlSource.FromTable<TableAttribute,ColumnAttribute,Poi>(connString,SRS.Epsg4326,t => t.Name, c => c.Name,new string[]{"0,4,0.4","4,8,0.8","8,21,1.0"});
            var tile = source.GetTile(1,0,0,GridSet.Create(SRS.Epsg4326));
            Assert.IsTrue(tile.Length > 0);
        }
        [Test]
        public void TestSqlSourceFromTable3()
        {
            var source = SqlSource.FromTable<TableAttribute,ColumnAttribute,Poi>(connString,SRS.Epsg4326,t => t.Name, c => c.Name,new string[]{"0,4,0.4","4,8,0.8","8,21,1.0"});
            var tile = source.GetTile(0,0,0,GridSet.Create(SRS.Epsg3857));
            Assert.IsTrue(tile.Length > 0);
        }
        [Test]
        public void TestSqlSourceFromTable4()
        {
            var source = SqlSource.FromTable<TableAttribute,ColumnAttribute,Poi>(connString,SRS.Epsg4326,t => t.Name, c => c.Name,new string[]{"0,4,0.4","4,8,0.8","8,21,1.0"});
            var tile = source.GetTile(1,0,0,GridSet.Create(SRS.Epsg4490));
            Assert.IsTrue(tile.Length > 0);
        }
        private static string SqlFunc(int x,int y,int z,GridSet gridSet)
        {
            var gridSet3857 = GridSet.Create(SRS.Epsg3857);
            var bbox = gridSet3857.GetTileBBox(0,0,0);
            var minx = bbox[0];
            var miny = bbox[1];
            var maxx = bbox[2];
            var maxy = bbox[3];
            var envelop = $"ST_MakeEnvelope({minx},{miny},{maxx},{maxy}, 4326)";
            return "WITH mvtgeom AS (\n" +
                    "	SELECT\n" +
                    $"		ST_AsMVTGeom ( geom,{envelop} , extent => 4096, buffer => 64 ) AS geom,\n" +
                    "		NAME\n" +
                    "		\n" +
                    "	FROM\n" +
                    "		points_of_interest \n" +
                    "	WHERE\n" +
                    $"		geom && {envelop}  and random() < 0.3\n" +
                    "	) SELECT\n" +
                    "	ST_AsMVT ( mvtgeom.* ) \n" +
                    "FROM\n" +
                    "	mvtgeom;";
        }
    }
}