using System.IO;
using System.Reflection;
using NUnit.Framework;
using TinyTile.Layers;
using TinyTile.Sources;
using TinyTile.Utils;

namespace TInyTile.Tests
{

    [TestFixture]
    public class PGisSqlSourceTest
    {
        [Test]
        public void Test()
        {
            
        }
    }
    [TestFixture]
    public class MBTileSourceTest
    {
        [Test]
        public void TestXyz()
        {
            var path = Path.Combine(AssemblyPath, "TestDatas", "points.mbtiles");
            var source = new MbTile(path);
            
            var gs = GridSet.Create(SRS.Epsg3857);
            var tile = source.GetTile(0,0,0,gs);
            Assert.IsTrue(tile.Length > 0);
        }
        private static string AssemblyPath
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().Location;
                var dir = Path.GetDirectoryName(codeBase);

                return dir;
            }
        }
    }
}