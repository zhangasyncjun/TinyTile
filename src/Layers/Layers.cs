using TinyTile.Sources;
using TinyTile.Utils;

namespace TinyTile.Layers
{
    public class Layer : ILayerable
    {
        public Layer(string name,ISourceable source,TileFormat? format = null,GridSet gridSet = null, BoundingBox boundingBox = null)
        {
            Name = name;
            BoundingBox = boundingBox ?? source.BoundingBox;
            Format = format ?? source.TileFormat;
            GridSet = gridSet ?? GridSet.Create(source.SRS);
            Source = source;
        }

        public string Name { get; set; }
        public BoundingBox BoundingBox { get; set; }
        public TileFormat Format { get; set; }
        public GridSet GridSet { get; set; }
        public ISourceable Source { get; set; }


        public byte[] GetTile(int x, int y, int z)
        {
            //todo if source not support this format,try use the format source support then transform result to desired format
            return Source.GetTile(x, y, z, GridSet);
        }
    }
}