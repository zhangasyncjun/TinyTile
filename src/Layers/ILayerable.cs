using TinyTile.Sources;
using TinyTile.Utils;

namespace TinyTile.Layers
{
    public interface ILayerable
    {
        string Name { get; set; }
        BoundingBox BoundingBox { get; set; }
        TileFormat Format { get; set; }
        GridSet GridSet { get; set; }
        ISourceable Source { get; set; }
        byte[] GetTile(int x, int y, int z);
    }
}