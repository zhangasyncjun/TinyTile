using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using TinyTile.Utils;

namespace TinyTile.Sources
{
    public class MbTile : ISourceable
    {
        #region private region
        private readonly string _connString;
        private Dictionary<string, string> _metaData;
        private int _minZoom;
        private int _maxZoom;

        private Dictionary<string, string> ReadMeta(string _connString)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            using (var conn = new SqliteConnection(_connString))
            {
                conn.Open();
                using (var cmd = new SqliteCommand("SELECT name,value from metadata", conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var name = reader.IsDBNull(0) ? null : reader.GetString(0);
                            var value = reader.IsDBNull(1) ? null : reader.GetString(1);

                            results.Add(name, value);
                        }
                    }
                }
            }

            return results;
        }
        private static string _tileSql = "SELECT tile_data FROM tiles WHERE zoom_level = @z and tile_column = @x and tile_row = @y";
        #endregion
        public SRS SRS { get; set; }
        public TileFormat[] SupportedFormats { get; set; }
        public BoundingBox BoundingBox { get; set; }
        public TileFormat TileFormat { get; set; }

        public MbTile(string _filePath)
        {
            _connString = new SqliteConnectionStringBuilder
            {
                DataSource = _filePath,
                Mode = SqliteOpenMode.ReadOnly,
                Cache = SqliteCacheMode.Shared,
            }.ToString();
            _metaData = ReadMeta(_connString);
            SRS = SRS.Epsg3857;
            TileFormat = Enum.Parse<TileFormat>(_metaData["format"].ToUpper());
            SupportedFormats = new TileFormat[] { TileFormat };
            _metaData.TryGetValue("bounds", out string bounds);
            //todo parse bounds
            BoundingBox = new BoundingBox(-20037508.34, -20037508.34, 20037508.34, 20037508.34);

            _metaData.TryGetValue("minzoom", out string minZoomStr);
            _metaData.TryGetValue("maxzoom", out string maxZoomStr);

            _minZoom = string.IsNullOrEmpty(minZoomStr) ? 0 : int.Parse(minZoomStr);
            _maxZoom = string.IsNullOrEmpty(maxZoomStr) ? 0 : int.Parse(maxZoomStr);
        }
        public byte[] GetTile(int x, int y, int z, GridSet gridSet)
        {
            byte[] result = null;

            if(gridSet != null){
                if(gridSet.EPSG != 3857){
                    throw new NotSupportedException("only support SRS 3857");
                }
            }

            if(gridSet.YAxisSchema != YAxisSchema.Tms)
            {
                y = (1 << z) - y -1;
            }


            using (var conn = new SqliteConnection(_connString))
            {
                conn.Open();
                using (var cmd = new SqliteCommand(_tileSql, conn))
                {
                    cmd.Parameters.Add(new SqliteParameter() { ParameterName = "z", Value = z });
                    cmd.Parameters.Add(new SqliteParameter() { ParameterName = "x", Value = x });
                    cmd.Parameters.Add(new SqliteParameter() { ParameterName = "y", Value = y });

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result = (byte[])reader[0];
                        }
                    }
                }
            }
            return result;
        }

        public Task<byte[]> GetTileAsync(int x, int y, int z, GridSet gridSet)
        {
            return Task.FromResult(GetTile(x,y,z,gridSet));
        }
    }
}