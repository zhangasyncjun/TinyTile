using System.Threading.Tasks;
using TinyTile.Utils;

namespace TinyTile.Sources
{
    public interface ISourceable
    {
        SRS SRS { get; set; }
        TileFormat TileFormat { get; set; }
        BoundingBox BoundingBox { get; set; }
        byte[] GetTile(int x, int y, int z, GridSet gridSet);
        Task<byte[]> GetTileAsync(int x, int y, int z, GridSet gridSet);
    }
}