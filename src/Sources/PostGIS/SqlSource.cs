using System;
using TinyTile.Utils;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Npgsql;

namespace TinyTile.Sources
{
    //todo tidy up 
    public class SqlSource : ISourceable
    {
        public static SqlSource FromTable(string connString,string tableName)
        {
            throw new NotImplementedException();//todo 
        }
        public static SqlSource FromTable<TTableAttr,TColumnAttr,TModel>(string connString,SRS srs,Func<TTableAttr,string> tableNameSelector, Func<TColumnAttr,string> colNameSelector ,string[] filter = null) where  TTableAttr:Attribute where TColumnAttr:Attribute
        {
            var columnNames = new List<string>();
            string tableName = "";
            string geometryColumnName = "";
            var targetType = typeof(TModel);
            
            tableName = GetAttrStringProp<TTableAttr>(targetType,tableNameSelector);

            if(string.IsNullOrEmpty(tableName)){
                throw new Exception("column name or table name is empty/null");
            }
            var props = targetType.GetProperties();
            foreach (var prop in props)
            {
                var propType = prop.PropertyType;
                var colAttrs = prop.GetCustomAttributes(typeof(TColumnAttr),false);
                if(colAttrs.Length <= 0) continue;
                var colAttr = colAttrs[0] as TColumnAttr;
                var columnName = colNameSelector(colAttr);
                if(string.IsNullOrEmpty(columnName)) continue;
                var ntsgType = typeof(NetTopologySuite.Geometries.Geometry);
                bool isNtsg = propType == typeof(NetTopologySuite.Geometries.Geometry) || propType.BaseType == typeof(NetTopologySuite.Geometries.Geometry);
                if(isNtsg || propType.BaseType.IsInstanceOfType(typeof(NetTopologySuite.Geometries.Geometry))){
                    isNtsg = true;
                }
                if(isNtsg){
                    geometryColumnName = columnName;
                    continue;
                }

                columnNames.Add(columnName);
            }

            var sqlSource = new SqlSource(connString,GenerateSQLFunc(tableName,columnNames,geometryColumnName,filter,srs),srs);

            return sqlSource;
        }

        private static double? CalculateTreshold(List<(int min,int max,double filter)> filters,int z)
        {
            foreach (var f in filters)
            {
                var min = f.min;
                var max = f.max;

                if(z >= min && z < max) return f.filter;
            }
            return null;
        }

        private static Func<int, int, int, GridSet,string> GenerateSQLFunc(string tableName,IEnumerable<string> columnNames,string geomColumnName,string[] zoomFilters,SRS srs)
        {
            var filters = new List<(int min, int max, double filter)>();
            if(zoomFilters != null){
                foreach (var zoomF in zoomFilters)
                {
                    var items = zoomF.Split(",");
                    var min = int.Parse(items[0]);
                    var max = int.Parse(items[1]);
                    var value = double.Parse(items[2]);

                    filters.Add((min, max, value));
                }
            }
            

            Func<int, int, int, GridSet,string> sqlFunc = new Func<int, int, int, GridSet, string>((x,y,z,gs) => 
                GenSql(x,y,z,tableName,columnNames,geomColumnName,gs,srs,filters)
            );
            return sqlFunc;
        }

        private static string GenSql(int x, int y, int z, string tableName, IEnumerable<string> columnNames, string geomColumnName, GridSet gs,SRS sRS, List<(int min, int max, double filter)> filters)
        {
            double[] bbox = sRS.BBox;

            var tileBBox = gs.GetTileBBox(x,y,z);
            var minx = tileBBox[0];
            var miny = tileBBox[1];
            var maxx = tileBBox[2];
            var maxy = tileBBox[3];

            string tileEnvelop =   $"'SRID={gs.EPSG};POLYGON(({minx} {miny}, {maxx} {miny}, {maxx} {maxy}, {minx} {maxy}, {minx} {miny}))'";
            string geom;
            if(sRS.EPSG != gs.EPSG){
                geom = $"ST_AsMVTGeom(  st_transform({geomColumnName},{gs.EPSG})   ,  {tileEnvelop}::geometry,extent => 4096,buffer => 64       ) as geom";     
            }else{
                geom = $"ST_AsMVTGeom(  {geomColumnName}  ,  {tileEnvelop}::geometry,extent => 4096,buffer => 64       ) as geom";
            }
            string columns = geom + "," + string.Join(" , ", columnNames);
            string whereCondition = $" geom && {tileEnvelop} ";
            if (filters.Count > 0)
            {
                whereCondition += $" and random() < {CalculateTreshold(filters, z)} ";
            }

            string sql = $"WITH mvtgeom AS ( SELECT {columns} from {tableName} where {whereCondition} ) select ST_AsMVT(mvtgeom.*) from mvtgeom";
            return sql;
        }

        private static string GetAttrStringProp<TAttribute>(Type targetType,Func<TAttribute,string> selector) where TAttribute:Attribute
        {
            var attributeType = typeof(TAttribute);
            var attrs = targetType.GetCustomAttributes(typeof(TAttribute), true);
            var attr = attrs.FirstOrDefault() as TAttribute;
            if(attr == null){return null;}
            var attrPropName = selector(attr);

            return attrPropName;
        }


        private string _connString;
        private Func<int, int, int, GridSet,string> _sqlFunc;
        public SRS SRS { get; set; }
        public TileFormat TileFormat { get; set; }
        public TileFormat[] SupportedFormats { get; set; }
        public BoundingBox BoundingBox { get; set; }
        public SqlSource(string connString, Func<int, int, int, GridSet, string> sqlFunc, SRS sRS)
        {
            _connString = connString;
            _sqlFunc = sqlFunc;
            SRS = sRS;
            TileFormat = TileFormat.MVT;
            SupportedFormats = new TileFormat[]{TileFormat.PBF};
            BoundingBox = new BoundingBox(sRS.BBox[0], sRS.BBox[1], sRS.BBox[2], sRS.BBox[3]);
        }
        public byte[] GetTile(int x, int y, int z, GridSet gridSet)
        {
            byte[] result = null;
            var sql = _sqlFunc(x, y, z, gridSet);
            using (var conn = new NpgsqlConnection(_connString))
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandText = sql;
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    result = (byte[])reader[0];
                }
            }

            return result;
        }

        public async Task<byte[]> GetTileAsync(int x, int y, int z, GridSet gridSet)
        {
            byte[] result = null;
            var sql = _sqlFunc(x, y, z, gridSet);
            using (var conn = new NpgsqlConnection(_connString))
            {
                await conn.OpenAsync();
                var cmd = conn.CreateCommand();
                cmd.CommandText = sql;
                var reader = await cmd.ExecuteReaderAsync();
                if (reader.Read())
                {
                    result = (byte[])reader[0];
                }
            }

            return result;
        }
    }
}