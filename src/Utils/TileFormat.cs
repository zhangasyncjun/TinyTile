namespace TinyTile.Utils
{
    public enum TileFormat
    {
        PBF,//GZIP compressed MVT,see https://github.com/mapbox/mbtiles-spec/blob/master/1.3/spec.md
        MVT,//Mapbox Vector Tile Specification,see https://github.com/mapbox/vector-tile-spec/
        GeoJson,
        JPG,
        JPEG,
        PNG,
        WEBP,
    }
}