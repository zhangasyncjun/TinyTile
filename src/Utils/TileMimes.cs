namespace TinyTile.Utils
{
    public static class TileMimes
    {
        public static readonly string Pbf = "application/x-protobuf";
        public static readonly string Mvt = "application/vnd.mapbox-vector-tile";
    }
}