namespace TinyTile.Utils
{
    public class BoundingBox
    {
        public static BoundingBox WORLD4326 => new BoundingBox(-180.0, -90.0, 180.0, 90.0);
        public static BoundingBox WORLD3857 => new BoundingBox(-20037508.34, -20037508.34, 20037508.34, 20037508.34);

        public double MinX { get; set; }
        public double MinY { get; set; }
        public double MaxX { get; set; }
        public double MaxY { get; set; }
        public double Width => MaxX - MinX;
        public double Height => MaxY - MinY;
        public BoundingBox(){}
        public BoundingBox(double minX, double minY, double maxX, double maxY)
        {
            MinX = minX;
            MinY = minY;
            MaxX = maxX;
            MaxY = maxY;
        }
    }
}