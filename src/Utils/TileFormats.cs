using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TinyTile.Utils
{
    public static class TileFormats
    {
        public static List<TileFormat> Vector => new List<TileFormat>(){ TileFormat.PBF, TileFormat.MVT, TileFormat.GeoJson };
    }
}